<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Order = Order::all();

            $response = $Order;
            $code = 200;
        }catch (\Exception $e) {
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this ->validate($request, [
            'name_id' => 'required',
            'date' => 'required'
        ]);

        try {
            $Order = new Order();
            $Order->name_id = $request->name_id;
            $Order->date = $request->date;

            $Order->save();

            $code = 200;
            $response = $Order;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = "tidak ada data";
            } else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Order = Order::findOrFail($id);

            $code = 200;
            $response = $Order;
        }catch (\Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this ->validate($request, [
            'name_id' => 'required',
            'date' => 'required'
        ]);

        try {
            $Order = new Order();
            $Order->name_id = $request->name_id;
            $Order->date = $request->date;

            $Order->save();

            $code = 200;
            $response = $Order;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = "tidak ada data";
            } else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Order = Order::find($id);
            $Order->delete();

            $code = 200;
            $response = $Order;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }
}
