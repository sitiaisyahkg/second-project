<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Product = Product::all();

            $response = $Product;
            $code = 200;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
            'expired' => 'required',
            'value' => 'required',
            'price' => 'required'
        ]);

        try {
            $Product = new Product();
            $Product->name = $request->name;
            $Product->category_id = $request->category_id;
            $Product->expired = $request->expired;
            $Product->value = $request->value;
            $Product->price = $request->price;

            $Product->save();

            $code = 200;
            $response = $Product;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = "tidak ada data";
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Product = Product::findOrFail($id);

            $code = 200;
            $response = $Product;
        }catch (\Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 400;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
            'expired' => 'required',
            'value' => 'required',
            'price' => 'required'
        ]);

        try {
            $Product = new Product();
            $Product->name = $request->name;
            $Product->category_id = $request->category_id;
            $Product->expired = $request->expired;
            $Product->value = $request->value;
            $Product->price = $request->price;

            $Product->save();

            $code = 200;
            $response = $Product;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = "tidak ada data";
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Product = Product::find($id);
            $Product->delete();

            $code = 200;
            $response = $Product;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }
}
