<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Category = Category::all();

            $response = $Category;
            $code = 200;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required | min :3 | max:20;'
        ]);

        try {
            $Category = new Category();
            $Category->name = $request->name;

            $Category->save();

            $code = 200;
            $response = $Category;
        } catch (Exception $e) {
            if ($e instanceof ValidationException) {
                $code = 404;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Category = Category::all();

            $response = $Category;
            $code = 200;
        }catch (\Exception $e) {
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required | min :3 | max:20;'
        ]);

        try {
            $Category = new Category();
            $Category->name = $request->name;

            $Category->save();

            $code = 200;
            $response = $Category;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = "tidak ada data";
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Category = Category::find($id);
            $Category->delete();

            $code = 200;
            $response = $Category;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }
}
