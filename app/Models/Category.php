<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Category extends Model
{
    protected $table = 'category';

    use softDeletes;

    public function Product()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }
}
