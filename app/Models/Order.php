<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Order extends Model
{
    protected $table = 'order';

    use softDeletes;

    public function Product()
    {
        return $this->hasMany(Product::class, 'name', 'id', 'expired', 'category_id');
    }
}
