<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public function Category()
    {
        return $this->hasOne(Category::class, "id", "name");
    }

    public function Order()
    {
        return $this->hasOne(Order::class, "id", "name_id");
    }
}
